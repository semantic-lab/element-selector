# Element Selector
Element Selector is a Google Chrome extension.

When enable the extension, the DOM element will be highlight,
also while DOM element receive click event,
it will print the selector of event target by `consoloe.warn`.

## Install

1. Open [Chrome Extenstions Page (chrome://extensions/)](chrome://extensions/)
2. Enable Developer mode
3. click Load unpacked button
4. Selecte current project folder

> (Offical Reference)[https://developer.chrome.com/docs/extensions/mv3/getstarted/]


