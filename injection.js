window.pptr = {
    highlightTarget: undefined,
    ignoreClassNames: [ 'pptr-highlight', 'active', 'actived' ],
    lastSelector: undefined
};

function injectStyle() {
    const styleContent = `
        .pptr-highlight {
            background: #ff888844 !important;
        }
    `;

    const styleNode = document.createElement('style');
    styleNode.innerHTML = styleContent;
    document.getElementsByTagName('head')[0]
        .appendChild(styleNode);
}

function _removeHighlightingClassName(target) {
    const updateClassNames = (target.getAttribute('class') || '')
        .replace(' pptr-highlight', '');
    target.setAttribute('class', updateClassNames);
}

function highlightingHandler(event) {
    const { target } = event;

    // 1. remove last highlight
    const highlightAtOtherTarget = !!window.pptr.highlightTarget;
    if (highlightAtOtherTarget) {
        _removeHighlightingClassName(window.pptr.highlightTarget);
    }

    // 2. add highlight for current target node
    let updateClassNames = (target.getAttribute('class') || '')
        .split(' ');
    updateClassNames.push('pptr-highlight');
    updateClassNames = updateClassNames.join(' ');
    target.setAttribute('class', updateClassNames);

    // 3. update target node
    window.pptr.highlightTarget = target;
    event.stopPropagation();
}

function _selectorJoin(selectorPartials) {
    return selectorPartials
        .join(' > ')
        .replace(/ >  > /g, ' ')
        .replace(/^ > /g, '')
        .replace(/ > $/g, '')
        .replace(/  /, ' ');
}

function _hasUniqueNode(selectorArray = []) {
    const fullSelector = _selectorJoin(selectorArray);
    return [ ...document.querySelectorAll(fullSelector) ].length === 1;
}

function _validName(name) {
    const matchResult = name.match(/^[a-zA-Z]/g);
    return matchResult && matchResult.length > 0;
}

function _classNamesSelectorOptimize(selector) {
    const nodes = document.querySelectorAll(selector);
    const isUniqueSelector = nodes.length === 1;
    const node = nodes[0];

    const selectorPartials = selector.split(' > ');
    selectorPartials.forEach((selectorPartial, partialIndex) => {
        const hasClassName = selectorPartial.includes('.');
        if (hasClassName) {
            const [ element, nthChild ] = selectorPartial.split(':');
            const selectorMetas = element.split('.');
            const tag = selectorMetas[0];
            const classNames = selectorMetas.slice(1);
            const combinations = [];
            function generalCombinations(result, startIndex) {
                for (let i = startIndex; i < classNames.length; i++) {
                    const newCombination = [ ...result, classNames[i] ];
                    combinations.push(newCombination);
                    generalCombinations(newCombination, startIndex + 1);
                }
            }
            generalCombinations([], 0);
            combinations.sort((a, b) => a.length - b.length);
            combinations.some((combination) => {
                // 1. copy selectorPartials
                const copiedSelectorPartials = JSON.parse(JSON.stringify(selectorPartials));
                // 2.
                let selectorPartialToCheck = [tag, ...combination].join('.');
                if (!!nthChild) {
                    selectorPartialToCheck = `${selectorPartialToCheck}:${nthChild}`;
                }
                // 3.
                copiedSelectorPartials[partialIndex] = selectorPartialToCheck;
                // 4.
                const selectorToCheck = _selectorJoin(copiedSelectorPartials);
                // 5.
                const nodesToCheck = document.querySelectorAll(selectorToCheck);
                if (isUniqueSelector) {
                    if (nodesToCheck.length === 1 && node === nodesToCheck[0]) {
                        selectorPartials[partialIndex] = selectorPartialToCheck;
                        return true;
                    }
                } else {
                    if (node === nodesToCheck[0]) {
                        selectorPartials[partialIndex] = selectorPartialToCheck;
                        return true;
                    }
                }
            });
        }
    });

    return selectorPartials;
}


function _redundantSelectorOptimize(selectorPartials) {
    const node = document.querySelector(_selectorJoin(selectorPartials));
    for (let i = 0; i < selectorPartials.length; i++) {
        if (selectorPartials[i] !== undefined) {
            const copyPartials = JSON.parse(JSON.stringify(selectorPartials));
            copyPartials[i] = undefined;
            const newSelector = _selectorJoin(copyPartials);
            if (newSelector !== '') {
                const nodesOfNewSelector = document.querySelectorAll(newSelector);
                const currentPartialIsRedundant = nodesOfNewSelector.length === 1 && nodesOfNewSelector[0] === node;
                if (currentPartialIsRedundant) {
                    selectorPartials[i] = undefined;
                }
            }
        }
    }

    return selectorPartials;
}

function _cssSelector(element, pathElements, warningMessages) {
    if (!(element instanceof Element))
        return;

    let pathIndex = 1; // 0 equals to target element
    let path = [];
    while (element.nodeType === Node.ELEMENT_NODE) {
        const tag = element.localName;
        const isBodyOrHTML = tag === 'body' || tag === 'html';
        if (isBodyOrHTML) {
            break;
        }
        const id = element.getAttribute('id');
        let classNames = element.getAttribute('class');
        classNames = (classNames && classNames.length > 0)
            ? classNames
                .split(' ')
                .filter(className => _validName(className))
                .filter(className => !window.pptr.ignoreClassNames.includes(className))
                .map((className) => `.${className}`)
                .join('')
            : undefined;
        let selector = (id && _validName(id))
            ? `${tag}#${id}`
            : (classNames)
                ? `${tag}${classNames}`
                : tag;

        if (_hasUniqueNode([selector, ...path])) {
            path.unshift(selector);
            break;
        }

        const parentNode = element.parentNode || pathElements[pathIndex];
        if (!parentNode) {
            path.unshift(selector);
            break;
        }

        const siblingNodes = [ ...parentNode.childNodes ].filter(node => !['#text', '#comment'].includes(node.nodeName));
        let index = siblingNodes.indexOf(element);
        if (index === -1) {
            siblingNodes.some((node, i) => {
                if (node.textContent === element.textContent) {
                    index = i;
                    warningMessages.push(`%c ${tag}:nth-child(${index + 1}) is calculate by textContent, please double check if it's necessary.`);
                }
            });
        }
        selector = `${selector}:nth-child(${index + 1})`;
        path.unshift(selector);
        if (_hasUniqueNode([selector, ...path])) {
            break;
        }

        element = element.parentNode || pathElements[pathIndex];
        pathIndex++;
    }

    path = _classNamesSelectorOptimize(_selectorJoin(path));
    path = _redundantSelectorOptimize(path);

    return _selectorJoin(path);
}

function getSelector(event) {
    const highlightAtOtherTarget = !!window.pptr.highlightTarget;
    if (highlightAtOtherTarget) {
        let warningMessages = [];
        const selector = _cssSelector(event.target, event.path, warningMessages);
        if (window.pptr.lastSelector !== selector) {
            console.warn(`\`${selector}\``);
            window.pptr.lastSelector = selector;
            warningMessages.forEach((msg) => console.warn(msg, `color: #ff8888;`));
            warningMessages = [];
        }
        // event.stopPropagation();
    }
}

function selectorCheck(selector, textMap) {
    const selectors = selector.split(' > ');
    const textMapKeys = Object.keys(textMap);
    selectors.forEach((selectorPartial, partialIndex) => {
        const isNthChildSelector = selectorPartial.includes(':nth-child');
        const textMapKey = selectorPartial
            .replace(/.*:nth-child\(/g, '')
            .replace(/\)/g, '');
        const hasTextMapKey = textMapKeys.includes(textMapKey);
        if (isNthChildSelector && hasTextMapKey) {
            const selectorPartialWithOutNth = selectorPartial.replace(/:nth-child.*/g, '');
            const selectorToCurrent = _selectorJoin([ ...selectors.slice(0, partialIndex), selectorPartialWithOutNth ]);
            [ ...document.querySelectorAll(selectorToCurrent) ]
                .some((node, nodeIndex) => {
                    if (node.innerText.includes(textMap[textMapKey])) {
                        selectors[partialIndex] = `${selectorPartialWithOutNth}:nth-child(${nodeIndex + 1})`;
                        return true;
                    }
                });
        }
    });

    const resultSelector = _selectorJoin(selectors);
    const nodesOfResultSelector = [ ...document.querySelectorAll(resultSelector) ];

    console.log({
        selector: resultSelector,
        nodes: nodesOfResultSelector,
        isUnique: nodesOfResultSelector.length === 1,
    });
}

function querySelectorAll(selector) {
    return document.querySelectorAll(selector);
}

function parseSelector(selector) {
    const selectorPartials = selector
        .split(' > ')
        .reverse();
    selectorPartials.forEach((item, index) => {
        const selector = _selectorJoin(selectorPartials
            .slice(0, index + 1)
            .reverse());
        console.log(selector, document.querySelectorAll(selector));
    });
}

(function () {
    injectStyle();
    window.addEventListener('mouseover', (event) => {
        const { target } = event;
        highlightingHandler(event);
        target.addEventListener('click', getSelector);
        target.removeEventListener('mouseleave', getSelector);
    });
    window.addEventListener('click', getSelector);
})();

